CREATE TABLE flights
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  number      varchar(255) not null,
  date DATE,
  PRIMARY KEY (id)
);
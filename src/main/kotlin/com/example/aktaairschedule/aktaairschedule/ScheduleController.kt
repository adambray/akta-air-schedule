package com.example.aktaairschedule.aktaairschedule

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController("/api/v1/schedule")
class ScheduleController(val flightRepository: FlightRepository) {
    @GetMapping
    fun getSchedule(): Map<String, List<LocalDate>> {
        val flights = flightRepository.findAll()
        val results = HashMap<String, MutableList<LocalDate>>()

        flights.forEach {
            if (results.get(it.number) == null) {
                results.put(it.number, mutableListOf(it.date))
            } else {
                results.get(it.number)!!.add(it.date)
            }
        }

        return results
    }

    @PostMapping
    fun addFlight(@RequestBody flight: Flight) : String {
        flightRepository.save(Flight(number = flight.number, date = flight.date))
        return "Created"
    }
}
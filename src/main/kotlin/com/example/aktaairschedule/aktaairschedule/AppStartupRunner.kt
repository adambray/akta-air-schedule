package com.example.aktaairschedule.aktaairschedule

import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class AppStartupRunner(val flightRepository: FlightRepository) : CommandLineRunner {
    override fun run(vararg args: String?) {
        listOf("KS 1000", "KS 1001", "KS 2001").forEach {
            flightRepository.save(Flight(number = it, date = LocalDate.now().plusDays(1)))
            flightRepository.save(Flight(number = it, date = LocalDate.now().plusDays(2)))
            flightRepository.save(Flight(number = it, date = LocalDate.now().plusDays(3)))
            flightRepository.save(Flight(number = it, date = LocalDate.now().plusDays(4)))
            flightRepository.save(Flight(number = it, date = LocalDate.now().plusDays(5)))
        }
    }

}
package com.example.aktaairschedule.aktaairschedule

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AktaAirScheduleApplication

fun main(args: Array<String>) {
	runApplication<AktaAirScheduleApplication>(*args)
}


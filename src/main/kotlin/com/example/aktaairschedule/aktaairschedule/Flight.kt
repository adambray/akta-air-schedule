package com.example.aktaairschedule.aktaairschedule

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "flights")
data class Flight(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        val number: String,

        val date: LocalDate
)

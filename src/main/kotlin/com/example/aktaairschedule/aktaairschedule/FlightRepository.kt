package com.example.aktaairschedule.aktaairschedule

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface FlightRepository: CrudRepository<Flight, Long>{
    override fun findAll(): List<Flight>
}
